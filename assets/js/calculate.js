var suggest = JSON.parse(localStorage.getItem('suggest'));
var correct = JSON.parse(localStorage.getItem('correct'));

var correctAnswers = 0;
var percentage;
var area = localStorage.getItem('area');

document.getElementById("area").innerHTML = area;

function autocomplete(inp, arr) {
    var currentFocus;
    inp.addEventListener("input", function(e){
       var a, b, i, val = this.value;

       closeAllLists();
       if (!val) {
           return false;
       }
       currentFocus = -1;

       a = document.createElement("DIV");
       a.setAttribute("id", this.id + "autocomplete-list");
       a.setAttribute("class", "autocomplete-items");

       this.parentNode.appendChild(a);

       for (i = 0; i < arr.length; i++) {
           if (arr[i].substr(0, val.length).toUpperCase() === val.toUpperCase()) {
               b = document.createElement("DIV");

               b.innerHTML = "<strong>" + arr[i].substr(0, val.length); + "</strong>";
               b.innerHTML += arr[i].substr(val.length);
               b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    b.addEventListener("click", function (e) {
                        inp.value = this.getElementsByTagName("input")[0].value;

                        closeAllLists();
                    });
               a.appendChild(b);
           }
       }
    });

    inp.addEventListener("keydown", function (e) {
       var x = document.getElementById(this.id + "autocomplete-list");
       if (x) {
           x = x.getElementsByTagName("div");
       }

       if (e.keyCode == 40) { //arrow down key
           currentFocus++;
           addActive(x);
       } else if ( e.keyCode == 38 ) { //arrow up key
           currentFocus--;
           addActive(x);
       } else if (e.keyCode == 13) {
           e.preventDefault();
           if (currentFocus > -1) {
               if (x) {
                   x[currentFocus].click();
               }

           }
       }
    });

    function addActive(x) {
        if (!x) {
            return false
        }

        removeActive(x);
        if (currentFocus >= x.length) {
            currentFocus = 0;
        }
        if (currentFocus < 0) {
            currentFocus = (x.length - 1);
        }
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(element) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (element != x[i] && element != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (ev) {
       closeAllLists(ev.target);
    });
}

/*Validate*/
var chosen = Array();
var i = 0;
function addCity () {
    var cityChosen = document.getElementById("cityInput").value;

    var index = chosen.indexOf(cityChosen);
    var comp = suggest.indexOf(cityChosen);

    if (comp <= -1) {
        document.getElementById("err").innerHTML = "Grad ne postoji u listi";
        document.getElementById("err").style.color = "red";
        return;
    }
    if (index > -1) {
        return document.getElementById("err").innerHTML = "Vec ste uneli taj grad";
    }
     else {
        document.getElementById("err").innerHTML = "";
    }
    if (cityChosen.length > 1) {
        var li = "<li id='li" + i+ "' onclick='removeCity(id)'>" + cityChosen + "<span class='remove'>"+"x"+"</span>"+"</li>";

        var liEl = document.getElementById("list");
        liEl.setAttribute("class", "box-shadow");

        liEl.innerHTML += li;
        chosen.push(cityChosen);
        console.log(chosen);
    }
    ++i;
    document.getElementById("cityInput").value = '';
}
/*Remove city from ul list and array*/
function removeCity(id){
    var c = document.getElementById(id);
    var cc = c.innerText;

    var name = cc.substring(0, cc.length -1);

    var index = chosen.indexOf(name);
    if (index > -1) {
        chosen.splice(index, 1);
    }
    c.parentNode.removeChild(c);

    document.getElementById("err").innerHTML = "Uspesno ste obrisali grad " + name;
    document.getElementById("err").style.color = "green";
    document.getElementById("cityInput").innerHTML = "";
}
/*End of Test - Progress bar*/
function endQuiz() {
    correctAnswers = 0;
    chosen.filter(function (chosen) {
        if (correct.indexOf(chosen) > -1) {
            correctAnswers++;
        }
    });
    percentage = (100*correctAnswers)/(correct.length);
    localStorage.setItem("perc", percentage);
    localStorage.setItem("corr", correctAnswers);
    localStorage.setItem("chsnLngth", chosen.length);

    window.location.replace("chart.html");
}

function progressAnimation() {
    var elem = document.querySelector('aria-valuenow');
    var elemClass = document.getElementById('animate');
    var pos = 0;
    var id = setInterval(frame, 5);
    var p = localStorage.getItem('perc');
    function frame() {
        if (pos == p) {
            clearInterval(id);
        } else {
            pos++;
            elem = pos;
            elemClass.style.width = pos + '%';
        }
    }
    var percentage = localStorage.getItem('perc') + "%";
    var chosenLength = localStorage.getItem('chsnLngth');
    var correct = localStorage.getItem('corr');
    var setChosenValue = '';

    if (chosenLength == 1) {
        setChosenValue = chosenLength + ' grad';
    }
    else if (chosenLength >= 2 && chosenLength < 5){
        setChosenValue = chosenLength + ' grada';
    }
    else if (chosenLength > 4) {
        setChosenValue = chosenLength + " gradova";
    }
    else if (chosenLength === 1) {
        setChosenValue = chosenLength + " grad";
    }
    else {
        setChosenValue = "Niste izabrali nijedan grad";
    }

    function getNoun(num) {
        if(!num) return ' gradova';
        if(num === 1) return ' grad';
        if(num >=2 && num < 5) return ' grada';

        return ' gradova';
    }

    document.getElementById("crrct").innerText = correct + getNoun(correct);;
    document.getElementById("chsn").innerText = setChosenValue;
    document.getElementById('prcntg').innerText = percentage;
}



