function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (seconds <= 10 && minutes < 1){
            document.getElementById("time").style.color = "red";
        }
        if (seconds == 0 && minutes == 0) {
            return endQuiz();
        }
        if (--timer < 0) {
            timer = duration;
        }
    }, 1000)
}

window.onload = function () {
    var time = localStorage.getItem('time'),
        display = document.querySelector('#time');
    startTimer(time, display);
};