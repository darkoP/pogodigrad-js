function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                console.log(JSON.parse(allText));
                var data = JSON.parse(allText);

                localStorage.setItem('suggest', JSON.stringify(data.ponudjene));
                localStorage.setItem('correct', JSON.stringify(data.tacno));
                localStorage.setItem('time', data.vreme);
                localStorage.setItem('area', data.oblast);


            }
        }
    }
    rawFile.send(null);
}