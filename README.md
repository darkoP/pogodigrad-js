Projekat pokrenuti:

1. git clone https://darkoP@bitbucket.org/darkoP/pogodigrad-js.git
2. Otvoriti index.html u pretrazivacu koji koristite.

Ukoliko igru testirate u chrome-u, potrebno je uraditi sledece:
Pokrenete komandu run i ukucate chrome --allow-file-access-from-files
(Ovo je potrebno uraditi jer chrome po default-u ne dozvoljava rad sa lokalnim fajlom na racunaru). Detaljno objasnjenje mozete pronaci na linku: https://www.chromium.org/for-testers/command-line-flags